# Package

version       = "0.1.0"
author        = "fgsfds"
description   = "IRC bot for #doom2d"
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
bin           = @["d2dfbot"]

# Dependencies

requires "nim >= 0.17.0"
requires "irc >= 0.1.0"
