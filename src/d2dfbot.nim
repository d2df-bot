import irc, strutils, tables, times, threadpool, net
import config, commands, buildsys

var refreshTime: float = epochTime() + 100.0

var client = newIrc(ircHost, ircPort.Port,
                    nick = ircNick, user = ircNick, realname = ircNick,
                    joinChans = @[ircChannel])
client.connect()

while true:
  checkBuildFinished(client)

  if epochTime() > refreshTime:
    if masterRefresh:
      checkServerChanges(client)
    if buildAutoRebuild:
      checkRebuildNeeded(client)
    refreshTime = epochTime() + masterRefreshTime

  var event: IrcEvent
  if client.poll(event, 100):
    case event.typ
    of EvConnected:
      discard
    of EvDisconnected, EvTimeout:
      break
    of EvMsg:
      if event.cmd == MPrivMsg:
        checkMemos(client, event)
        let msg = event.params[event.params.high]
        if msg.startsWith(".:") and msg.len > 2:
          let argv = msg[2 .. ^1].splitWhitespace()
          let cmd = argv[0]
          let args = argv[1 .. ^1].join(" ")
          if cmd in botCommands:
            echo("command: ", cmd, " ", args)
            botCommands[cmd](client, event, args)
      echo(event.raw)
