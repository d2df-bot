import strutils, tables
import enet
import config, rawread

type
  ServerInfo* = object
    address*: string
    name*: string
    map*: string
    mode*: string
    players*: string
    password*: bool
    build*: uint8
    playerCount: uint8
    updated: bool

var serverList* = initTable[string, ServerInfo](16)
var serverListChanged* = false
var serverListNewServer* = ""
var serverListNewPlayer* = ""
var firstUpdate = true

var maxNameLen: int = 0
var maxMapLen: int = 0

var netAddr: enet.TAddress
var netPeer: enet.PPeer
var netClient: enet.PHost
var netEvent: enet.TEvent
var netConnected: bool = false

proc `==`*(a, b: ServerInfo): bool {.inline.} =
  result = a.address == b.address and
           a.name == b.name and
           a.map == b.map and
           a.mode == b.mode and
           a.players == b.players and
           a.build == b.build and
           a.password == b.password

proc pwdToString(p: bool): string {.inline.} =
  if p: result = "p: yes"
  else: result = "p: no"

proc `$`*(a: ServerInfo): string {.inline.} =
  result = "$1 | $2 | $3 | $4 | v$5 | $6" % [
    a.name & " ".repeat(max(0, maxNameLen - a.name.len)),
    a.map & " ".repeat(max(0, maxMapLen - a.map.len)),
    a.mode & " ".repeat(max(0, 4 - a.mode.len)),
    a.players & " ".repeat(max(0, 5 - a.players.len)),
    $a.build,
    pwdToString(a.password)
  ]

proc newPlayer(a, b: ServerInfo): bool {.inline.} =
  b.playerCount > a.playerCount

proc gameModeToString(m: uint8): string {.inline.} =
  case m
  of 1: result = "dm"
  of 2: result = "tdm"
  of 3: result = "ctf"
  of 4: result = "coop"
  else: result = "???"

proc readServerInfo(): ServerInfo =
  result = ServerInfo()
  result.address = readString() & ":" & $readWord()
  result.name = readString()
  result.map = readString()
  result.mode = gameModeToString(readByte())
  result.playerCount = readByte()
  result.players = $result.playerCount & "/" & $readByte()
  result.build = readByte()
  result.password = readByte() == 1
  result.updated = true

proc masterConnect(): bool =
  if netConnected: return false

  netClient = enet.createHost(nil, 1, 2, 0, 0)
  if netClient == nil:
    echo("masterConnect(): could not create host")
    return false

  if enet.setHost(addr netAddr, masterAddress) != 0:
    echo("masterConnect(): could not set host")
    return false
  netAddr.port = masterPort

  netPeer = netClient.connect(addr netAddr, 2, 0)
  if netPeer == nil:
    echo("masterConnect(): could not connect to host")
    netClient.destroy()
    return false

  netConnected = false
  while not netConnected:
    if netClient.hostService(addr netEvent, 2000) > 0 and netEvent.kind == enet.EvtConnect:
      netConnected = true
    else:
      echo("connection to master failed")
      netPeer.reset()
      netClient.destroy()
      return false

  return true

proc masterDisconnect() =
  if not netConnected: return
  netPeer.disconnect(0)
  netClient.flush()
  netPeer.reset()
  netClient.destroy()
  netConnected = false

proc updateServerList*(): bool =
  if not masterConnect():
    return false
  defer: masterDisconnect()

  var sendMsg: uint8 = 202
  var sendPck = enet.createPacket(addr sendMsg, 1, enet.FlagReliable)
  discard netPeer.send(0.cuchar, sendPck)

  result = false
  while netClient.hostService(addr netEvent, 3000) > 0:
    if netEvent.kind == enet.EvtReceive and netEvent.packet.dataLength > 1:
      withRawBuffer(netEvent.packet.data):
        if readByte() != 202:
          continue
        result = true
        defer: firstUpdate = false
        maxNameLen = 0
        maxMapLen = 0
        var num = readByte()
        if num > 0.uint8:
          for i in 1..num:
            let serv = readServerInfo()
            serverList.withValue(serv.address, oserv) do:
              let changed = oserv[] != serv
              let newPlayers = newPlayer(oserv[], serv)
              if changed: serverListChanged = true
              if newPlayers and not firstUpdate:
                serverListNewPlayer = serv.address
            do:
              serverListChanged = true
              if not firstUpdate:
                serverListNewServer = serv.address
            serverList[serv.address] = serv
      for k, v in serverList.mpairs():
        if v.updated:
          v.updated = false
          if v.name.len > maxNameLen:
            maxNameLen = v.name.len
          if v.map.len > maxMapLen:
            maxMapLen = v.map.len
        else:
          serverList.del(k)
      break

proc releaseServerList*() =
  serverList.clear()
  if netConnected:
    masterDisconnect()
  enet.deinitialize()

if enet.initialize() != 0:
  quit("could not init ENet!")
