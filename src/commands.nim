import irc, strutils, tables, threadpool
import config, utils, master, buildsys

type
  BotCommandProc* = proc (irc: Irc, ev: IrcEvent, args: string)

var botCommands*: Table[string, BotCommandProc]
var memos = initTable[string, seq[tuple[s: string, m: string]]]()

proc checkServerChanges*(irc: Irc) =
  if updateServerList():
    if serverListNewServer != "":
      serverList.withValue(serverListNewServer, s) do:
        irc.privmsg(ircChannel, "new server: '$1'" % [s.name])
      serverListNewServer = ""
    if serverListNewPlayer != "":
      serverList.withValue(serverListNewPlayer, s) do:
        irc.privmsg(ircChannel, "player joined '$1' ($2)" % [s.name, s.players])
      serverListNewPlayer = ""

proc checkBuildFinished*(irc: Irc) =
  if buildFinished:
    sync()
    if buildSuccess:
      irc.privmsg(buildOrigin, "build finished. binaries available @ " & buildLink)
    else:
      irc.privmsg(buildOrigin, "build failed")
    buildFinished = false

proc checkRebuildNeeded*(irc: Irc) =
  if not buildInProgress and shouldUpdate():
    irc.privmsg(ircChannel, "commit to $1 detected ($2), rebuilding" % [buildGitPath, lastCommitMsg])
    buildOrigin = ircChannel
    spawn attemptBuild()

proc checkMemos*(irc: Irc, ev: IrcEvent) =
  if ev.nick in memos:
    for memo in memos[ev.nick]:
      let (s, m) = memo
      irc.privmsg(ev.origin, "$1: message from $2: $3" % [ev.nick, s, m])
    memos.del(ev.nick)

proc cmdVersion(irc: Irc, ev: IrcEvent, args: string) =
  irc.privmsg(ev.origin, "#doom2d bot ver. " & botVersion & ", by fgsfds")

proc cmdCommands(irc: Irc, ev: IrcEvent, args: string) =
  var s = ""
  for k in botCommands.keys():
    s &= " " & k
  irc.privmsg(ev.origin, "available commands:" & s)

proc cmdServerlist(irc: Irc, ev: IrcEvent, args: string) =
  if not updateServerList():
    irc.privmsg(ev.origin, "couldn't fetch fresh list, using stored list.")
  if serverList.len == 0:
    irc.privmsg(ev.origin, "server list is empty.")
  else:
    var n = serverList.len
    irc.privmsg(ev.origin, "got $1 $2:" % [$n, "server".plural(n)])
    for v in serverList.values():
      irc.privmsg(ev.origin, $v)
  serverListNewPlayer = ""
  serverListNewServer = ""

proc cmdBuild(irc: Irc, ev: IrcEvent, args: string) =
  if buildInProgress:
    irc.privmsg(ev.origin, "already building")
  else:
    getLastCommitMsg(true)
    irc.privmsg(ev.origin, "attempting to build d2df-sdl ($1)..." % [lastCommitMsg])
    buildOrigin = ev.origin
    spawn attemptBuild()

proc cmdEcho(irc: Irc, ev: IrcEvent, args: string) =
  irc.privmsg(ev.origin, "$1: $2" % [ev.nick, args])

proc cmdRoll(irc: Irc, ev: IrcEvent, args: string) =
  var nargs = args.strip()
  if nargs == "": nargs = "1d6"
  irc.privmsg(ev.origin, "$1: $2" % [ev.nick, diceResult(nargs)])

proc cmdMemo(irc: Irc, ev: IrcEvent, args: string) =
  let argv = args.strip().splitWhitespace()
  if argv.len < 2:
    irc.privmsg(ev.origin, "expected at least 2 words")
    return
  let nick = argv[0].strip()
  if nick == "":
    irc.privmsg(ev.origin, "expected non-empty nick")
    return
  if nick in memos:
    memos[nick].add((ev.nick, argv[1 .. ^1].join(" ")))
  else:
    memos[nick] = @[(ev.nick, argv[1 .. ^1].join(" "))]
  irc.privmsg(ev.origin, "$1: message will be delivered to $2 when they say something" % [ev.nick, nick])

botCommands = initTable[string, BotCommandProc](16)
botCommands["version"] = cmdVersion
botCommands["commands"] = cmdCommands
botCommands["serverlist"] = cmdServerlist
botCommands["build"] = cmdBuild
botCommands["echo"] = cmdEcho
botCommands["roll"] = cmdRoll
botCommands["memo"] = cmdMemo
