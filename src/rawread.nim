type
  ByteBuffer {.unchecked.} = array[0..1, uint8]

var rawBase: ptr ByteBuffer = nil
var rawPtr: uint = 0

proc readByte*(): uint8 {.inline.} =
  result = rawBase[rawPtr]
  rawPtr += 1

proc readWord*(): uint16 {.inline.} =
  result = cast[ptr uint16](addr rawBase[rawPtr])[]
  rawPtr += 2

proc readLongWord*(): uint32 {.inline.} =
  result = cast[ptr uint32](addr rawBase[rawPtr])[]
  rawPtr += 4

proc readString*(): string {.inline.} =
  var slen = rawBase[rawPtr]
  rawPtr += 1
  result = ""
  if slen == 0: return
  for i in rawPtr ..< rawPtr + slen:
    result.add(rawBase[i].char)
  rawPtr += slen

template withRawBuffer*(buf: pointer, body: untyped) =
  rawBase = cast[ptr ByteBuffer](buf)
  rawPtr = 0
  body
