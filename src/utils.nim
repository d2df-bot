import strutils, random, strscans, times

proc diceResult*(s: string): string =
  var numDice = 1
  var numSides = 6
  if s.scanf("$id$i", numDice, numSides):
    result = "["
    var sum = 0
    if numDice < 1: numDice = 1
    if numDice > 10: numDice = 10
    if numSides < 1: numSides = 1
    for i in 1 .. numDice:
      let r = 1 + random(numSides)
      sum += r
      result &= $r
      if i < numDice:
        result &= ", "
    result &= "] = " & $sum
  else:
    result = "expected dice string (#d#)"

proc plural*(s: string, count: SomeInteger): string =
  if count == 1: s else: s & "s"

proc dateString*(): string =
  getTime().getLocalTime().format("dd/MM/yyyy")

proc timeString*(): string =
  getTime().getLocalTime().format("HH:mm:ss")
