import os, ospaths, strutils, parsecfg

let botVersion* = "0.0.1"
when defined(windows):
  let dataPath* = getAppDir()
else:
  let dataPath* = "~/.d2dfbot"

var ircHost* = "irc.quakenet.org"
var ircPort*: uint16 = 6667
var ircChannel* = "#doom2d"
var ircNick* = "d2dfbot"
var buildPath* = dataPath / "build"
var buildTempPath* = dataPath / "temp"
var buildGitPath* = "git://repo.or.cz/d2df-sdl.git"
var buildCommand* = "build.bat"
var buildHeadlessCommand* = "build_headless.bat"
var buildCleanCommand* = "clean.bat"
var buildUploadCommand* = "upload.bat"
var buildCompressCommand* = "zip -r latest.zip . -x \".*\" -x \"*/.*\""
var buildCompressBinaryCommand* = "zip -r latest.zip . -x \".*\""
var buildOutputBinary* = "Doom2DF.exe"
var buildOutputHeadless* = "Doom2DF_H.exe"
var buildOutputArchive* = "latest.zip"
var buildOutputBinaryArchive* = "latest_bin.zip"
var buildLink* = "http://doom2d.org/doom2d_forever/latest.zip"
var buildAutoRebuild* = true
var masterAddress*: string = "mpms.doom2d.org"
var masterPort*: uint16 = 25665
var masterRefreshTime*: float = 30.0
var masterRefresh*: bool = true

let rootDir* = getCurrentDir()
let projDir* = "d2df-sdl"

# load #

block:
  var cfg: Config = nil
  try:
    cfg = loadConfig(dataPath / "config.cfg")
  except:
    quit("could not open config file: " & getCurrentExceptionMsg())

  if cfg != nil:
    ircHost = cfg.getSectionValue("IRC", "server")
    ircPort = cfg.getSectionValue("IRC", "port").parseUInt().uint16
    ircChannel = cfg.getSectionValue("IRC", "channel")
    ircNick = cfg.getSectionValue("IRC", "nick")
    masterAddress = cfg.getSectionValue("ServerList", "masterAddress")
    masterPort = cfg.getSectionValue("ServerList", "masterPort").parseUInt().uint16
    masterRefreshTime = cfg.getSectionValue("ServerList", "refreshTime").parseFloat()
    if masterRefreshTime < 0.0: masterRefresh = false
    if masterRefreshTime < 1.0: masterRefreshTime = 1.0
    buildPath = cfg.getSectionValue("BuildSystem", "outputPath")
    buildTempPath = cfg.getSectionValue("BuildSystem", "tempPath")
    buildGitPath = cfg.getSectionValue("BuildSystem", "gitRepo")
    buildCommand = cfg.getSectionValue("BuildSystem", "buildCommand")
    buildCleanCommand = cfg.getSectionValue("BuildSystem", "cleanCommand")
    buildHeadlessCommand = cfg.getSectionValue("BuildSystem", "headlessCommand")
    buildCompressCommand = cfg.getSectionValue("BuildSystem", "compressCommand")
    buildCompressBinaryCommand = cfg.getSectionValue("BuildSystem", "compressBinCommand")
    buildUploadCommand = cfg.getSectionValue("BuildSystem", "uploadCommand")
    buildOutputBinary = cfg.getSectionValue("BuildSystem", "outputBin")
    buildOutputHeadless = cfg.getSectionValue("BuildSystem", "outputHeadlessBin")
    buildOutputArchive = cfg.getSectionValue("BuildSystem", "outputArchive")
    buildOutputBinaryArchive = cfg.getSectionValue("BuildSystem", "outputBinArchive")
    buildLink = cfg.getSectionValue("BuildSystem", "outputLink")
