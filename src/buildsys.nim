import os, osproc, ospaths, strutils, strscans
import config, utils

var buildFinished* = false
var buildSuccess* = false
var buildInProgress* = false
var buildOrigin* = ""
var buildVersion* = "999"
var lastCommitMsg* = ""

when defined(windows):
  let toNull = "" # " > nul 2>&1"
else:
  let toNull = "" # " > /dev/null 2>&1"

proc getLastCommitMsg*(f: bool) =
  if f: setCurrentDir(buildTempPath / projDir)
  if execShellCmd("git fetch") == 0:
    let (outp, errC) = execCmdEx("""git log -1 --pretty=format:"%h %B" origin/master""")
    lastCommitMsg = outp.strip()
  if f: setCurrentDir(rootDir)

proc shouldUpdate*(): bool =
  result = false
  if not existsDir(buildTempPath / projDir):
    return
  if not existsDir(buildTempPath / projDir / ".git"):
    return
  defer: setCurrentDir(rootDir)
  try:
    setCurrentDir(buildTempPath / projDir)
    let (outp, errC) = execCmdEx("git remote show origin")
    if errC != 0: return
    result = outp.contains("master (local out of date)")
    if result: getLastCommitMsg(false)
  except:
    discard

proc getProjectVersion(): string =
  let fname = "." / "game" / "g_net.pas"
  result = buildVersion
  if not existsFile(fname):
    return
  var i: int = 0
  for ln in lines(fname):
    if ln.strip().scanf("NET_PROTOCOL_VER = $i;", i):
      return $i

proc writeProjectVersion() =
  #try:
    var f = open("version.txt", fmWrite)
    defer: f.close()
    f.writeLine("$1 $2 $3" % [buildVersion, dateString(), timeString()])
  #except:
    discard

proc attemptBuild*() {.thread.} =
  buildInProgress = true
  buildFinished = false
  buildSuccess = false
  echo("starting build attempt")
  defer:
    setCurrentDir(rootDir)
    echo("build attempt finished")
    buildFinished = true
    buildInProgress = false
  try:
    setCurrentDir(buildTempPath)
    if existsDir(projDir) and existsDir(projDir / ".git"):
      setCurrentDir(projDir)
      echo("updating sources...")
      if execShellCmd("git pull" & toNull) != 0:
        return
    else:
      if existsDir(projDir):
        removeDir(projDir)
      echo("pulling sources...")
      if execShellCmd("git clone \"" & buildGitPath & "\"" & toNull) != 0:
        return
      setCurrentDir(projDir)
      if not existsDir("bin"):
        createDir("bin")
      if not existsDir("tmp"):
        createDir("tmp")
    discard execShellCmd(buildCleanCommand & toNull)
    if execShellCmd(buildCommand & toNull) != 0:
      return
    discard execShellCmd(buildCleanCommand & toNull)
    discard execShellCmd(buildHeadlessCommand & toNull)
    setCurrentDir("bin")
    if not existsFile(buildOutputBinary):
      return
    if existsFile(buildOutputArchive):
      removeFile(buildOutputArchive)
    if execShellCmd(buildCompressCommand & toNull) != 0:
      return
    discard execShellCmd(buildCompressBinaryCommand & toNull)
    copyFileWithPermissions(buildOutputArchive, buildPath / buildOutputArchive)
    removeFile(buildOutputArchive)
    try:
      copyFileWithPermissions(buildOutputBinaryArchive, buildPath / buildOutputBinaryArchive)
      removeFile(buildOutputBinaryArchive)
    except:
      discard
    try:
      setCurrentDir(".." / "src")
      buildVersion = getProjectVersion()
    except:
      discard
    setCurrentDir(buildPath)
    echo("version ", buildVersion)
    writeProjectVersion()
    if execShellCmd(buildUploadCommand & toNull) != 0:
      return
    buildSuccess = true
  except:
    echo("build failed: ", getCurrentExceptionMsg())
    buildSuccess = false
